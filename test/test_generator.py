import os
import shutil
from unittest import TestCase
from pathlib import Path
from src.Generator import Generator
import logging
import sys

logging.basicConfig(level=logging.DEBUG, stream=sys.stdout, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


class TestGenerator(TestCase):

    out_dir = '/home/eouser/test'

    def setUp(self):
        self.gen = Generator(
            input_dir='/eodata/Sentinel-1/SAR/SLC/2015/07/20/', 
            output_dir=self.out_dir
        )
        self.assertIsInstance(self.gen, Generator)

    def tearDown(self):
        shutil.rmtree(self.out_dir)

    def test_modify_template(self):
        benchfile = str(Path(os.path.dirname(__file__)) / "test_output.xml")
        with open(benchfile, 'r') as f:
            benchmark = f.read()

        input1= str(Path(self.gen.input_dir)/ 'S1A_IW_SLC__1SDV_20150720T203442_20150720T203512_006899_009522_4E7A')
        input2= str(Path(self.gen.input_dir) /'S1A_IW_SLC__1SDV_20150801T203443_20150801T203513_007074_009A02_3CD7')
        output= str(Path(self.out_dir, 'S1A_IW_SLC__1SDV_20150720T203442_20150720T203512_006899_009522_4E7A_Orb_Stack_Ifg_Deb_Flt') )
        repl = {
            "INPUT1":input1,
            "INPUT2":input2,
            "SNAP_FOLDER":'/home/eouser/test/snaphu',
            "OUTPUT":output
        }

        wf = self.gen._modify_template(1, 'interferogram_old.xml', repl)
        with open(wf, 'r') as f:
            data = f.read()

        self.maxDiff = None
        self.assertEqual(data, benchmark)

    def test_get_snaphu_cmd(self):
        expected = ['snaphu', '-f', '/home/eouser/pycharm/test/snaphu.conf', '/home/eouser/pycharm/test/Phase_ifg_VH_15Oct2018_08Nov2018.snaphu.img','21170']
        (cmd, out) = self.gen._get_snaphu_cmd(os.path.dirname(__file__))
        self.assertListEqual(cmd, expected)

        expected = '/home/eouser/pycharm/test/UnwPhase_ifg_VH_15Oct2018_08Nov2018.snaphu.img'
        self.assertEqual(out, expected)

    def test_run_snap(self):
        self.gen._run_snap(1, 'test_graph.xml', {})

    def test_create_image_pairs(self):
        pass

    def test_phase_unwrap(self):
        out = self.gen._phase_unwrap(1, '/home/eouser/processing-test/S1A_IW_SLC__1SDV_20150720T203442_20150720T203512_006899_009522_4E7A_Orb_Stack_Ifg_Deb_Flt/')
        expected = out = '/home/eouser/processing-test/S1A_IW_SLC__1SDV_20150720T203442_20150720T203512_006899_009522_4E7A_Orb_Stack_Ifg_Deb_Flt/UnwPhase_ifg_IW1_VH_20Jul2015_01Aug2015.snaphu.img'
        self.assertIsNotNone(out)

