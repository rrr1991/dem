"""Class to generate DEM using SNAP
"""
import os
import re
import shutil
import time
import logging
from collections import  namedtuple
from string import  Template
from pathlib import Path
from subprocess import  PIPE, run as sp_run
from multiprocessing import Process
from pyroSAR.snap.auxil import gpt
from src.LogPipe import LogPipe

Job = namedtuple('Job', ['id', 'thread'])


class Generator:
    """DEM generator class
    
    :param input_dir: input directory
    :type input_dir: str
    :param output_dir: output directory
    :type output_dir: str
    :param snaphu: location of snaphu directory
    :type snaphu: str
    :param logger: logger, defaults to None
    :type logger: logging.Logger, optional
    """

    TEMPLATEPATH= str(Path(os.path.dirname(__file__)) / "templates")
    snaphutimeout = 7200
    timeout     = 86400
    interfero   = 1
    phase       = 2
    dem         = 3

    def __init__(self, input_dir:str, output_dir:str, logger:logging.Logger=None):
        """Constructor
        """
        self.jobs = []
        self.logger = logger if logger else logging.getLogger('Generator')
        self.input_dir = Path(input_dir)
        self.output_dir = Path(output_dir)
        self.job_dir = Path(output_dir, 'jobs')

        if self.job_dir.exists(): shutil.rmtree(self.job_dir)
        self.job_dir.mkdir(parents=True)

    def run(self) -> bool:
        """Run generator
        
        :raises TimeoutError: when runtime exceeds timeout
        :return: `True` if successfull
        :rtype: bool
        """

        id = 0
        for i in self._create_image_pairs():
            id += 1
            self._run_workflow(id, i[0], i[1])
            # p = Process(target=self._run_workflow,args=(id, i[0], i[1],) )
            #
            # self.jobs.append(Job(id=id, thread=p))

        start_time = time.time()
        try:
            while True:
                runtime = time.time() - start_time
                if runtime > self.timeout:
                    raise TimeoutError("Process runtime exceeded allowed runtime: " + str(self.timeout))

                self._check_jobs_complete()
                self.logger.debug("number of running jobs: " + str(len(self.jobs)))

                # if running jobs empty and flagged to stop, stop controller
                if len(self.jobs) == 0:
                    self.logger.info("Processes complete")
                    break

                time.sleep(2)

            self.logger.info(f"Process complete, runtime: {runtime}")
            return True

        except TimeoutError as e:
            self.logger.error(str(e))
            self.stop()
            return False

        except Exception as e:
            self.logger.exception("Failed with exception: " + str(e))
            self.stop()
            return False

    def stop(self) -> bool:
        """Stop generator

        :return: `True`
        :rtype: bool
        """
        self.logger.info("Stopping generator")

        for ik in range(len(self.jobs)):
            j = self.jobs.pop()
            j.thread.terminate()
            time.sleep(2)
            if j.thread.is_alive():
                self.logger.debug(f"Forcefully killing job:{j.id}")
                j.thread.kill()

        return True

    def _check_jobs_complete(self) -> None:
        """Check if tasks complete
        """

        for i in range(len(self.jobs)):
            job = self.jobs.pop()
            
            if job.thread.is_alive():
                self.jobs.insert(0, job)

    def _create_image_pairs(self) -> list:
        """create image pairs

        :yield: list of image pair (['path1', 'path2])
        :rtype: Iterator[list]
        """
        yield [
            "/home/eouser/processing/input_imagery/S1B_IW_SLC__1SDV_20181015T030431_20181015T030458_013157_0184FE_C1D3.zip",
            "/home/eouser/processing/input_imagery/S1B_IW_SLC__1SDV_20181108T030430_20181108T030458_013507_018FE7_C3AC.zip"
        ]

    # @ray.remote
    def _run_workflow(self, id:int, input1:str, input2:str) -> None:
        """Run workflow
        """

        self.logger.info(f"Generating workflow id: {id}")
        self.logger.debug(f"Running step 1")
        out = self._interfero_1(id, input1, input2)
        
        self.logger.debug(f"Running step 2")
        out = self._interfero_2(id, out)

        self.logger.info(f"Generating phase unwrap for workflow id: {id}")
        out = self._phase_unwrap(id, out)

        self.logger.info(f"Converting to DEM for workflow id: {id}")
        out = self._convert_to_elevation(id, out)

    def _interfero_1(self, id:int, input1:str, input2:str) -> str:
        """Generate intial interferogram

        :param id: job id
        :type id: int
        :param input1: image 1 file path
        :type input1: str
        :param input2: image2 file path
        :type input2: str
        :return: output filepath
        :rtype: str
        """
        logger = self.logger.getChild(f'InterFero1.Job{id}')
        logger.info(f"Preparing inteferogram step 1")
        replacements = {
            "INPUT1": str(Path(input1).absolute()),
            "INPUT2": str(Path(input2).absolute()),
            "OUTPUT": str(self.job_dir / str(id) / Path(input1).stem) + '_Orb_Stack'
        }

        self._run_snap(id, 'interferogram_step1.xml', replacements)

        return replacements['OUTPUT']

    def _interfero_2(self, id:int, input:str) -> str:
        """Generate intial interferogram

        :param id: job id
        :type id: int
        :param input1: image 1 file path
        :type input1: str
        :param input2: image2 file path
        :type input2: str
        :return: output filepath
        :rtype: str
        """
        logger = self.logger.getChild(f'InterFero2.Job{id}')
        logger.info(f"Preparing inteferogram step 2")
        replacements = {
            "INPUT": str(Path(input).absolute()),
            "OUTPUT": str(self.job_dir / str(id) / Path(input).stem) + '_Ifg_Deb_Flt'
        }

        self._run_snap(id, 'interferogram_step2.xml', replacements)

        return replacements['OUTPUT']

    def _phase_unwrap(self, id:int, input:str) -> str:
        """Submit processed interferogram to be phase unwrapped

        :param id: job id
        :type id: int
        :param input: input dir
        :type input: str
        :return: output file path
        :rtype: str
        """
        logger = self.logger.getChild(f'PhaseUnwrap.Job{id}')
        (prog, output) = self._get_snaphu_cmd(input)

        start = time.time()
        logger.info(f"Submitting snaphu job: {prog}")
        lp = LogPipe(logger, logging.DEBUG)
        # try:
        process = sp_run(args=prog, stdout=lp, stderr=PIPE, timeout=self.snaphutimeout )
        lp.close()

        logger.info(f'Process complete with exit code {process.returncode}, runtime: {time.time() - start}')
        if (process.stderr):
            raise RuntimeError(f'errors encontered\n{process.stderr}')
        
        # except Exception as e:
        #     logger.exception(f"Could not run snaphu job")

        return output

    def _convert_to_elevation(self, id:int, input:str) -> str:
        """Submit phase unwrapped image to be converted to elevation

        :param id: job id
        :type id: int
        :param input: input dir
        :type input: str
        :return: output file path
        :rtype: str
        """
        logger = self.logger.getChild(f'Convert.Job{id}')
        logger.info(f"Preparing to convert phase to dem")

        replacements = {
            "INPUT": str(Path(input.replace('img', 'hdr')).absolute()),
            "OUTPUT": str(self.job_dir / str(id) )
        }

        self._run_snap(id, 'conversion.xml', replacements)

        return replacements['OUTPUT']

    def _get_snaphu_cmd(self, dir:str) -> tuple:
        """Extract snaphu command from .conf file

        :param filepath: dir of snaphu.conf file
        :type filepath: str
        :return: (snaphu cli cmd, output img file)
        :rtype: tuple
        """
        with open(str(Path(dir,'snaphu.conf')), 'r') as f:
            s = f.read()

        # matches = re.findall(r'(?<=\s)([a-zA-Z0-9_.]*)(?=\.img|\.conf|\.log)',s, re.MULTILINE)
        # for i in matches:
        #     s.replace(i, str(Path(dir, i)))
        s = re.sub(r'(?<=\s)(?=([a-zA-Z0-9_.]*\.)(img|conf|log))', dir+'/', s, re.MULTILINE)

        cmd = re.search(r'(?<=^#)(\s*)(snaphu -f.*)(?=\n)', s, re.MULTILINE).group(2)
        output = re.search(r'(?<=^OUTFILE)(\s*)(.*\.img)(?=\n)', s, re.MULTILINE).group(2)

        with open(str(Path(dir,'snaphu.conf')), 'w') as f:
            f.write(s)

        return (cmd.split(' '), output)

    def _run_snap(self, id:int, templname:str, replacements:dict) -> None:
        """Run snap gpt using pyroSAR

        :param templname: template file name
        :type templname: str
        :param replacements: dict containing template replacements
        :type replacements: dict
        """
        logger = self.logger.getChild(f'Snap.Job{id}')
        wf = self._modify_template(id, templname, replacements)
        logger.info(f"Submitting snap job: {id}, file: {wf}")
        gpt(wf)

        # except Exception as e:
        #     logger.exception(f"Could not run gpt")

    def _modify_template(self, id:int, templname:str, replacements:dict) -> str:
        """generate snap workflow
        
        :param id: job id
        :type id: id
        :param templname: template file name
        :type templname: str
        :param replacements: dict containing template replacements
        :type replacements: dict
        :return: snap workflow xml filepath
        :rtype: str
        """

        logger = self.logger.getChild(f'ModifyTemplate.Job{id}')


        # Edit template file
        with open(str(Path(self.TEMPLATEPATH, templname)), 'r') as tmplfile:
            tmpl = Template(tmplfile.read())

        workflow = tmpl.safe_substitute(replacements)

        # write job
        xml = str(self.job_dir /str(id)/ templname)
        Path(xml).parent.mkdir(parents=True, exist_ok=True)
        logger.debug(f"Writing workflow to: {xml}")
        with open(xml, 'w') as f:
            f.write(workflow)

        return xml
