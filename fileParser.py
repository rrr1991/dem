import os

input1= 'S1A_IW_SLC__1SDV_20150720T203442_20150720T203512_006899_009522_4E7A'
# input1 = 'S1A_IW_SLC__1SDV_20150720T162954_20150720T163023_006897_009510_8EAC'
input2= 'S1A_IW_SLC__1SDV_20150801T203443_20150801T203513_007074_009A02_3CD7'

def find(name, path):
    for root, dirs, files in os.walk(path):
        folder = name+'.SAFE'
        if folder in dirs:
            return os.path.join(root, folder)

path = '/eodata/Sentinel-1/SAR/SLC/2015/07/20/'

dir = os.getcwd()
path_parent = os.path.abspath(os.curdir)
os.chdir("../../..")

file = find(input1,path)

# for root, dirs, files in os.walk('/eodata/Sentinel-1'):
#    for name in files:
#       print(os.path.join(root, name))
